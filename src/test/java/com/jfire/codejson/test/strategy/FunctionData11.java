package com.jfire.codejson.test.strategy;

import java.util.ArrayList;

public class FunctionData11
{
    private ArrayList<String> list = new ArrayList<>();
    
    public FunctionData11()
    {
        list.add("12");
        list.add("45");
    }
    
    public ArrayList<String> getList()
    {
        return list;
    }
    
    public void setList(ArrayList<String> list)
    {
        this.list = list;
    }
    
}
